package epamjava.xml.xmltask.validators;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class TouristVouchersXMLValidator {
    private String xmlFileLocation;
    private String xsdFileLocation;

    public TouristVouchersXMLValidator(String xmlFileLocation, String xsdFileLocation) {
        this.xmlFileLocation = xmlFileLocation;
        this.xsdFileLocation = xsdFileLocation;
    }

    public void validate() throws IOException, SAXException {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        File schemaLocation = new File(xsdFileLocation);
        Schema schema = factory.newSchema(schemaLocation);
        Validator validator = schema.newValidator();
        Source source = new StreamSource(xmlFileLocation);
        validator.validate(source);
    }
}
