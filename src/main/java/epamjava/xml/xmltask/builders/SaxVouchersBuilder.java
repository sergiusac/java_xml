package epamjava.xml.xmltask.builders;

import epamjava.xml.xmltask.handlers.VoucherSaxHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;

public class SaxVouchersBuilder extends AbstractVouchersBuilder {
    private static final Logger LOGGER = LogManager.getLogger(SaxVouchersBuilder.class);

    private VoucherSaxHandler handler;
    private XMLReader reader;

    @Override
    protected void prepare() {
        handler = new VoucherSaxHandler();
        try {
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(handler);
        } catch (SAXException e) {
            LOGGER.error("Error: ", e);
        }
    }

    @Override
    public void buildVouchers(String fileName) {
        try {
            reader.parse(fileName);
        } catch (IOException | SAXException e) {
            LOGGER.error("Error: ", e);
        }
        vouchersList = handler.getVouchers();
    }
}
