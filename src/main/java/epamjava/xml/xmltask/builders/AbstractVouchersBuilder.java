package epamjava.xml.xmltask.builders;

import epamjava.xml.xmltask.entities.TouristVoucher;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractVouchersBuilder {
    protected List<TouristVoucher> vouchersList;

    public AbstractVouchersBuilder() {
        vouchersList = new ArrayList<>();
        prepare();
    }

    public AbstractVouchersBuilder(List<TouristVoucher> vouchers) {
        this.vouchersList = vouchers;
        prepare();
    }

    abstract public void buildVouchers(String fileName);

    public List<TouristVoucher> getVouchers() {
        return vouchersList;
    }

    abstract protected void prepare();

}
