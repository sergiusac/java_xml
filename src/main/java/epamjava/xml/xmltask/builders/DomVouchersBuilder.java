package epamjava.xml.xmltask.builders;

import epamjava.xml.xmltask.entities.TouristVoucher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class DomVouchersBuilder extends AbstractVouchersBuilder {
    private static final Logger LOGGER = LogManager.getLogger(DomVouchersBuilder.class);

    private DocumentBuilder documentBuilder;

    @Override
    protected void prepare() {
        try {
            documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            LOGGER.error("Error: ", e);
        }
    }

    @Override
    public void buildVouchers(String fileName) {
        Document document = null;

        try {
            document = documentBuilder.parse(fileName);
            Element root = document.getDocumentElement();
            NodeList vouchers = root.getElementsByTagName("touristVoucher");
            for (int i = 0; i < vouchers.getLength(); i++) {
                Element voucherElement = (Element) vouchers.item(i);
                vouchersList.add(buildVoucher(voucherElement));
            }
        } catch (SAXException | IOException e) {
            LOGGER.error("Error: ", e);
        }
    }

    private TouristVoucher buildVoucher(Element voucherElement) {
        TouristVoucher voucher = new TouristVoucher();
        voucher.setVoucherId(voucherElement.getAttribute("voucherId"));
        voucher.setType(getElementTextContent(voucherElement, "type"));
        voucher.setCountry(getElementTextContent(voucherElement, "country"));
        voucher.setTransport(getElementTextContent(voucherElement, "transport"));
        voucher.setNumberOfDays(Integer.parseInt(getElementTextContent(voucherElement, "numberOfDays")));
        return voucher;
    }

    private String getElementTextContent(Element element, String elementName) {
        return element.getElementsByTagName(elementName).item(0).getTextContent();
    }
}
