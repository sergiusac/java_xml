package epamjava.xml.xmltask.builders;

public enum VouchersBuilderTypes {
    SAX, STAX, DOM
}
