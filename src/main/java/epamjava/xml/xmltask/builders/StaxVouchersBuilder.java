package epamjava.xml.xmltask.builders;

import epamjava.xml.xmltask.entities.TouristVoucher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class StaxVouchersBuilder extends AbstractVouchersBuilder {
    private static final Logger LOGGER = LogManager.getLogger(StaxVouchersBuilder.class);

    private XMLInputFactory inputFactory;
    private TouristVoucher currentVoucher;

    @Override
    protected void prepare() {
        inputFactory = XMLInputFactory.newInstance();
    }

    @Override
    public void buildVouchers(String fileName) {
        FileInputStream inputStream = null;
        XMLStreamReader reader = null;

        try {
            inputStream = new FileInputStream(new File(fileName));
            reader = inputFactory.createXMLStreamReader(inputStream);

            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    if ("touristVoucher".equals(reader.getLocalName())) {
                        currentVoucher = new TouristVoucher();
                        currentVoucher.setVoucherId(reader.getAttributeValue(null, "voucherId"));
                        vouchersList.add(buildVoucher(reader));
                    }
                }
            }

        } catch (XMLStreamException | FileNotFoundException e) {
            LOGGER.error("Error: ", e);
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();;
                }
            } catch (IOException e) {
                LOGGER.error("Error: ", e);
            }
        }
    }

    private TouristVoucher buildVoucher(XMLStreamReader reader) throws XMLStreamException {
        while (reader.hasNext()) {
            int type = reader.next();

            if (type == XMLStreamConstants.START_ELEMENT) {
                String localName = reader.getLocalName();

                if ("type".equals(localName)) {
                    currentVoucher.setType(reader.getElementText());
                } else if ("country".equals(localName)) {
                    currentVoucher.setCountry(reader.getElementText());
                } else if ("transport".equals(localName)) {
                    currentVoucher.setTransport(reader.getElementText());
                } else if ("numberOfDays".equals(localName)) {
                    currentVoucher.setNumberOfDays(Integer.parseInt(reader.getElementText().trim()));
                }
            } else if (type == XMLStreamConstants.END_ELEMENT) {
                String localName = reader.getLocalName();

                if ("touristVoucher".equals(localName)) {
                    break;
                }
            }
        }

        return currentVoucher;
    }
}
