package epamjava.xml.xmltask;

import epamjava.xml.xmltask.builders.*;
import epamjava.xml.xmltask.entities.TouristVoucher;
import epamjava.xml.xmltask.factories.VouchersBuilderFactory;
import epamjava.xml.xmltask.validators.TouristVouchersXMLValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    private static String xmlLocation = "input" + File.separator + "touristVouchers.xml";
    private static String xsdLocation = "input" + File.separator +"touristVouchers.xsd";

    public static void main(String[] args) {
        TouristVouchersXMLValidator validator = new TouristVouchersXMLValidator(xmlLocation, xsdLocation);
        boolean isValid = false;
        try {
            validator.validate();
            isValid = true;
        } catch (SAXException | IOException e) {
            LOGGER.error("Failed to validate XML: ", e);
        }
        if (isValid) {
            System.out.println("XML file is valid");

            for (VouchersBuilderTypes type : VouchersBuilderTypes.values()) {
                List<TouristVoucher> vouchers = readVouchersXml(VouchersBuilderFactory.create(type));

                File outputFile = new File("output" + File.separator + type.name() + ".txt");
                outputFile.getParentFile().mkdirs();
                try {
                    outputFile.createNewFile();
                    printParsedVouchers(vouchers, outputFile);
                } catch (IOException e) {
                    LOGGER.error("Failed to make output: ", e);
                }
            }
        }
    }

    private static List<TouristVoucher> readVouchersXml(AbstractVouchersBuilder vouchersBuilder) {
        vouchersBuilder.buildVouchers(xmlLocation);
        return vouchersBuilder.getVouchers();
    }

    private static void printParsedVouchers(List<TouristVoucher> vouchers, File outputFile) throws FileNotFoundException {
            PrintWriter printWriter = new PrintWriter(outputFile);
            for (TouristVoucher voucher : vouchers) {
                printWriter.println(voucher);
            }
            printWriter.close();
    }
}
