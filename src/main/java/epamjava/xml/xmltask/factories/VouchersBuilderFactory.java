package epamjava.xml.xmltask.factories;

import epamjava.xml.xmltask.builders.*;

public class VouchersBuilderFactory {

    public static AbstractVouchersBuilder create(VouchersBuilderTypes type) {
        switch (type) {
            case DOM:
                return new DomVouchersBuilder();
            case SAX:
                return new SaxVouchersBuilder();
            case STAX:
                return new StaxVouchersBuilder();
            default:
                throw new EnumConstantNotPresentException(type.getDeclaringClass(), type.name());
        }
    }
}
