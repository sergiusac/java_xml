package epamjava.xml.xmltask.entities;

public class TouristVoucher {
    private String voucherId;
    private String type;
    private String country;
    private String transport;
    private int numberOfDays;

    public TouristVoucher() {
    }

    public TouristVoucher(String voucherId, String type, String country, String transport, int numberOfDays) {
        this.voucherId = voucherId;
        this.type = type;
        this.country = country;
        this.transport = transport;
        this.numberOfDays = numberOfDays;
    }

    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    @Override
    public String toString() {
        return "TouristVoucher{" +
                "voucherId='" + voucherId + '\'' +
                ", type='" + type + '\'' +
                ", country='" + country + '\'' +
                ", transport='" + transport + '\'' +
                ", numberOfDays=" + numberOfDays +
                '}';
    }
}
