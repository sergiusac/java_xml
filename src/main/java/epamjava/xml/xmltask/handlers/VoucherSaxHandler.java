package epamjava.xml.xmltask.handlers;

import epamjava.xml.xmltask.entities.TouristVoucher;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class VoucherSaxHandler extends DefaultHandler {

    private List<TouristVoucher> vouchers = new ArrayList<>();
    private TouristVoucher currentVoucher;
    private String currentElement;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if ("touristVoucher".equals(localName)) {
            currentVoucher = new TouristVoucher();
            currentVoucher.setVoucherId(attributes.getValue("voucherId"));
        } else if ("type".equals(localName)) {
            currentElement = "type";
        } else if ("country".equals(localName)) {
            currentElement = "country";
        } else if ("transport".equals(localName)) {
            currentElement = "transport";
        } else if ("numberOfDays".equals(localName)) {
            currentElement = "numberOfDays";
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if ("touristVoucher".equals(localName)) {
            vouchers.add(currentVoucher);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String textContent = new String(ch, start, length);

        if ("type".equals(currentElement)) {
            currentVoucher.setType(textContent);
        } else if ("country".equals(currentElement)) {
            currentVoucher.setCountry(textContent);
        } else if ("transport".equals(currentElement)) {
            currentVoucher.setTransport(textContent);
        } else if ("numberOfDays".equals(currentElement)) {
            currentVoucher.setNumberOfDays(Integer.parseInt(textContent.trim()));
        }

        currentElement = null;
    }

    public List<TouristVoucher> getVouchers() {
        return vouchers;
    }
}
